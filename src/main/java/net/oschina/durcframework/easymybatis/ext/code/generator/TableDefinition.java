/*
 * Copyright 2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.oschina.durcframework.easymybatis.ext.code.generator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 数据库表定义,从这里可以获取表名,字段信息
 */
public class TableDefinition {
	private String schema;
	private String tableName; // 表名
	private String comment; // 注释
	private List<ColumnDefinition> columnDefinitions = Collections.emptyList(); // 字段定义

	public TableDefinition() {
	}

	public TableDefinition(String tableName) {
		this.tableName = tableName;
	}
	
	/**
	 * 返回表字段
	 * @return
	 */
	public List<ColumnDefinition> getTableColumns() {
		List<ColumnDefinition> columns = this.getColumnDefinitions();
		
		List<ColumnDefinition> ret = new ArrayList<>();
		
		for (ColumnDefinition columnDefinition : columns) {
			if(!columnDefinition.isTransient()) {
				ret.add(columnDefinition);
			}
		}
		
		return ret;
	}
	
	/**
	 * 返回所有定义的字段
	 * @return
	 */
	public List<ColumnDefinition> getAllColumns() {
		return this.getColumnDefinitions();
	}

	/**
	 * 是否含有时间字段
	 * @return
	 */
	public boolean getHasDateField() {
		List<ColumnDefinition> columns = getColumnDefinitions();
		for (ColumnDefinition definition : columns) {
			if("Date".equals(definition.getJavaType())) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * 是否含有BigDecimal字段
	 * @return
	 */
	public boolean getHasBigDecimalField() {
		List<ColumnDefinition> columns = getColumnDefinitions();
		for (ColumnDefinition definition : columns) {
			if("BigDecimal".equals(definition.getJavaType())) {
				return true;
			}
		}
		return false;
	}
	

	public ColumnDefinition getPkColumn() {
		for (ColumnDefinition column : columnDefinitions) {
			if (column.getIsPk()) {
				return column;
			}
		}
		return null;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public List<ColumnDefinition> getColumnDefinitions() {
		return columnDefinitions;
	}

	public void setColumnDefinitions(List<ColumnDefinition> columnDefinitions) {
		this.columnDefinitions = columnDefinitions;
	}

	public String getSchema() {
		return schema;
	}

	public void setSchema(String schema) {
		this.schema = schema;
	}

}
